package main

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"log"
	"os"
)

func main() {
	// TODO Make via flags to skip CHECK_SUM filepath passing
	if len(os.Args) < 2 {
		log.Fatal("Please provide one or two arguments: CHECK_FILE [СHECKSUM_FILE] [RESULT_FILE]")
	}
	
	filename := os.Args[1]

	file, err := os.Open(filename)
	if err != nil && err != io.EOF {
		log.Print("Err when open file")
		log.Printf("\tpathfile: %v", filename)
		log.Fatal(err)
	}

	file_checksum := sha256.New()
	_, err = io.Copy(file_checksum, file)
	if err != nil {
		log.Print("Err when read file to hash")
		log.Fatal(err)
	}

	checksum_string := hex.EncodeToString(file_checksum.Sum(nil))

	var checksum_filename string
	if len(os.Args) >= 3 {
		checksum_filename = os.Args[2]
	} else {
		checksum_filename = "sha256.txt"
	}

	checksum_from_txt, err := os.ReadFile(checksum_filename)
	if err != nil && err != io.EOF {
		log.Print("Err when read file")
		log.Printf("\tpathfile: %v", checksum_filename)
		log.Fatal(err)
	}

	var write_filename string
	if len(os.Args) >= 4 {
		write_filename = os.Args[3]
	} else {
		write_filename = "result.txt"
	}
	
	write_file, err := os.Create(write_filename)
	if err != nil {
		log.Print("Err when create filename")
		log.Printf("\tfilename: %v", write_filename)
		log.Fatal(err)
	}

	ans := "check error"
	if string(checksum_from_txt) == checksum_string {
		ans = "check complete"
	}

	_, err = write_file.Write([]byte(ans))
	if err != nil {
		log.Print("Err when read filename")
		log.Printf("\tfilename: %v", write_filename)
		log.Fatal(err)
	}
}