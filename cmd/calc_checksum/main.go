package main

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Please provide one or two arguments: CHECK_FILE [DESTINATION_FILE]")
	}
	
	filename := os.Args[1]

	file, err := os.Open(filename)
	if err != nil && err != io.EOF {
		log.Print("Err when open file")
		log.Printf("\tpathfile: %v", filename)
		log.Fatal(err)
	}

	file_checksum := sha256.New()
	_, err = io.Copy(file_checksum, file)
	if err != nil {
		log.Print("Err when read file to hash")
		log.Fatal(err)
	}

	checksum_string := make([]byte, hex.EncodedLen(file_checksum.Size()))
	hex.Encode(checksum_string, file_checksum.Sum(nil))

	var write_filename string
	if len(os.Args) >= 3 {
		write_filename = os.Args[2]
	} else {
		write_filename = "file2.bin"
	}

	write_file, err := os.Create(write_filename)
	if err != nil {
		log.Print("Err when create filename")
		log.Printf("\tfilename: %v", write_filename)
		log.Fatal(err)
	}


	_, err = write_file.Write(checksum_string)
	if err != nil {
		log.Print("Err when read filename")
		log.Printf("\tfilename: %v", write_filename)
		log.Fatal(err)
	}
}