package main

import (
	"io"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Please provide one or two arguments: FILE_WITH_CHECKSUM [DESTINATION_FILE]")
	}
	
	filename := os.Args[1]

	file_bytes, err := os.ReadFile(filename)
	if err != nil && err != io.EOF {
		log.Print("Err when read file")
		log.Printf("\tpathfile: %v", filename)
		log.Fatal(err)
	}

	file_checksum := file_bytes[len(file_bytes) - 64:]

	var write_filename string
	if len(os.Args) >= 3 {
		write_filename = os.Args[2]
	} else {
		write_filename = "sha256.txt"
	}

	write_file, err := os.Create(write_filename)
	if err != nil {
		log.Print("Err when create filename")
		log.Printf("\tfilename: %v", write_filename)
		log.Fatal(err)
	}

	_, err = write_file.Write(file_checksum[:])
	if err != nil {
		log.Print("Err when read filename")
		log.Printf("\tfilename: %v", write_filename)
		log.Fatal(err)
	}
}