# Integration tests pipline task

## First stage: base

> Используя любой язык программирования, создаёшь три контейнера, которые реализуют три микросервиса:
>
> 1. Контейнер берёт `file1.bin` с диска, считает для него хэш (например sha256) и создаёт `file2.bin`, к которому дописывается полученный хэш.
> 2. Контейнер берёт `file2.bin`, вытаскивает дописанный хэш и кладёт его в текстовый файл `sha256.txt`
> 3. Контейнер берёт `file1.bin`, считает для него sha256 и сравнивает его с содержимым `sha256.txt`:
>     - если совпадало, то создавал файл `result.txt` с текстом `"check complete"`
>     - если проверка не проходит - `"check error"`

### Services

#### [calc_checksum](cmd/calc_checksum/main.go)

```bash
go run cmd/calc_checksum/main.go FILE_TO_CHECK [DESTINATION_FILE]
```

Calculates sha256 of `FILE_TO_CHECK` and write it to `DESTINATION_FILE` (default filepath - `file2.bin`)

##### Container

> TODO file passing to container

```bash
docker build -t calc-checksum -f build/package/Dockerfile.calcChecksum .
docker run -it calc-checksum:latest file2.bin
```

#### [move_checksum](cmd/move_checksum/main.go)

```bash
go run cmd/move_checksum/main.go FILE_WITH_CHECKSUM [DESTINATION_FILE]
```

Copy sha256 from `FILE_WITH_CHECKSUM` and write it to `DESTINATION_FILE` (default filepath - `sha256.txt`)

##### Container

> TODO file passing to container

```bash
docker build -t move-checksum -f build/package/Dockerfile.moveChecksum .
docker run -it move-checksum:latest file2.bin
```

#### [compare_checksums](cmd/compare_checksums/main.go)

```bash
go run cmd/compare_checksums/main.go FILE_TO_CHECK [СHECKSUM_FILE] [RESULT_FILE]
```

Calculates sha256 of `FILE_TO_CHECK` and compare with checksum from `СHECKSUM_FILE` (default filepath - `sha256.txt`).

Then writes check state to `RESULT_FILE`: `check complete` if both checksums are equal. Otherwise `check error`.

##### Container

> TODO file passing to container

```bash
docker build -t compare-checksums -f build/package/Dockerfile.compareChecksums .
docker run -it compare-checksums:latest file2.bin
```

### (Test) Run app

"Simple" way to test service:

```bash
# Make test file 
echo "Let's test!" >> file1.bin

# Create volume & copy test file to it
docker volume create data-vol
# Hack to copy file from host to volume
docker run --rm -v $PWD:/source -v data-vol:/data -w /source alpine cp file1.bin /data

# Run every service
docker run -it -v data-vol:/data calc-checksum:latest /data/file1.bin /data/file2.bin
docker run -it -v data-vol:/data move-checksum:latest /data/file2.bin /data/sha256.txt
docker run -it -v data-vol:/data compare-checksums:latest /data/file1.bin /data/sha256.txt /data/result.txt

# Check result
docker run --rm -v data-vol:/data -w /data alpine cat result.txt
```

Shell script above already in `test_run.sh`

You can pass filepath to script to test service on other files:

```bash
./test_run path/to/file.txt
```
