#!/usr/bin/env bash

if [ ! $1 ]; then
  echo "Let's test!" >> file1.bin
  FILE_PATH=$PWD
  FILE_NAME="file1.bin"
else
  FILE_PATH="$( cd "$( dirname "$1" )" && pwd )"
  FILE_NAME="$( basename $1 )"
fi

VOLUME_NAME="test-data-vol"

if [ ! "$(docker volume ls | grep $VOLUME_NAME)" ]; then
  docker volume create $VOLUME_NAME
fi
# Hack to copy file from host to volume
docker run --rm -v $FILE_PATH:/source -v $VOLUME_NAME:/data -w /source alpine cp $FILE_NAME /data
if [ ! $1 ]; then
  rm $FILE_NAME
fi

docker run -it -v $VOLUME_NAME:/data calc-checksum:latest /data/$FILE_NAME /data/file2.bin
docker run -it -v $VOLUME_NAME:/data move-checksum:latest /data/file2.bin /data/sha256.txt
docker run -it -v $VOLUME_NAME:/data compare-checksums:latest /data/$FILE_NAME /data/sha256.txt /data/result.txt

docker run --rm -v $VOLUME_NAME:/data -w /data alpine cat result.txt